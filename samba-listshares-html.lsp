<% local view, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"editshare", "deleteshare"}, session) %>
<% htmlviewfunctions.displaycommandresults({"createshare"}, session, true) %>

<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Name</th>
		<th>Path</th>
		<th>Comment</th>
	</tr>
</thead><tbody>
<% local name = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,share in ipairs(view.value) do %>
	<% name.value = share.name %>
	<tr>
		<td>
		<% if viewlibrary.check_permission("editshare") then %>
			<% htmlviewfunctions.displayitem(cfe({type="link", value={name=name, redir=redir}, label="", option="Edit", action="editshare"}), page_info, -1) %>
		<% end %>
		<% if viewlibrary.check_permission("deleteshare") then %>
			<% htmlviewfunctions.displayitem(cfe({type="form", value={name=name}, label="", option="Delete", action="deleteshare"}), page_info, -1) %>
		<% end %>
		</td>
		<td><%= html.html_escape(share.name) %></td>
		<td><%= html.html_escape(share.path) %></td>
		<td><%= html.html_escape(share.comment) %></td>
	</tr>
<% end %>
</tbody></table>
<% if #view.value == 0 then %>
<p>No Shares Found</p>
<% end %>

<% if viewlibrary and viewlibrary.check_permission("createshare") then %>
	<% htmlviewfunctions.displayitem(cfe({type="link", value={redir=redir}, label="Create new share", option="Create Share", action="createshare"}), page_info, htmlviewfunctions.incrementheader(header_level)) %>
<% end %>
<% htmlviewfunctions.displaysectionend(header_level) %>
