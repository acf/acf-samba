local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.listfiles(self)
        return self.model.listconfigfiles()
end

function mymodule.expert(self)
	return self.handle_form(self, function() return self.model.getconfigfile(self.clientdata.filename) end, self.model.setconfigfile, self.clientdata, "Save", "Edit Samba File", "File Saved")
end

function mymodule.join(self)
	return self.handle_form(self, self.model.get_join, self.model.set_join, self.clientdata, "Join", "Join Domain")
end

function mymodule.listshares(self)
	return self.model.list_shares()
end

function mymodule.editshare(self)
	return self.handle_form(self, self.model.read_share, self.model.update_share, self.clientdata, "Save", "Edit Share", "Share Saved")
end

function mymodule.deleteshare(self)
	return self.handle_form(self, self.model.get_delete_share, self.model.delete_share, self.clientdata, "Delete", "Delete Share", "Share Deleted")
end

function mymodule.createshare(self)
	return self.handle_form(self, self.model.read_share, self.model.create_share, self.clientdata, "Create", "Create Share", "Share Created")
end

return mymodule
